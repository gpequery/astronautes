<?php

    namespace App\Controller;

    use App\Entity\Astronaute;
    use App\Handler\ResponseHandler;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\Routing\Annotation\Route;
    use App\Repository\AstronauteRepository;

    /**
     * @Route("/astronaute")
     */
    class AstronauteController extends AbstractController {
        /**
         * @Route("/", name="astronaute_get_all", methods={"GET"})
         * @param ResponseHandler      $responseHandler
         * @param AstronauteRepository $astronauteRepository
         *
         * @return JsonResponse
         */
        public function getAll(ResponseHandler $responseHandler, AstronauteRepository $astronauteRepository): JsonResponse {
            $astronauts = $astronauteRepository->findAll();
            $data = Astronaute::normalize($astronauts);

            return $this->json($responseHandler->createResponse(true, $data));
        }

        /**
         * @Route("/{id}", name="astronaute_get_id", methods={"GET"}, requirements={"id"="\d+"})
         * @param ResponseHandler      $responseHandler
         * @param AstronauteRepository $astronauteRepository
         *
         * @param                      $id
         *
         * @return JsonResponse
         */
        public function getById(ResponseHandler $responseHandler, AstronauteRepository $astronauteRepository, $id): JsonResponse {
            $astronaut = $astronauteRepository->find($id);

            if ($astronaut) {
                $data = $astronaut->toJson();

                return $this->json($responseHandler->createResponse(true, $data));
            }


            return $this->json($responseHandler->createResponse(false, "Astronaut $id not found"));
        }

        /**
         * @Route("/", name="astronaute_add", methods={"POST"})
         *
         * @param ResponseHandler $responseHandler
         * @param Request         $request
         *
         * @return JsonResponse
         */
        public function create(ResponseHandler $responseHandler, Request $request): JsonResponse {
            $name = $request->request->get('name');
            $age  = $request->request->getInt('age');

            if ($name && $age) {
                $astronaut = new Astronaute();
                $astronaut->setName($name)->setAge($age);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($astronaut);
                $entityManager->flush();

                $data = $astronaut->toJson();

                return $this->json($responseHandler->createResponse(true, $data));
            }

            return $this->json($responseHandler->createResponse(false, "Missing params name and/or age"));
        }

        /**
         * @Route("/{id}", name="astronaute_update_id", methods={"PUT"}, requirements={"id"="\d+"})
         * @param ResponseHandler      $responseHandler
         * @param Request              $request
         * @param AstronauteRepository $astronautRepository
         * @param integer              $id
         *
         * @return JsonResponse
         */
        public function update(ResponseHandler $responseHandler, Request $request, AstronauteRepository $astronautRepository, $id): JsonResponse {
            $astronaut = $astronautRepository->find($id);

            if ($astronaut) {
                $name = $request->request->get('name') ?? $astronaut->getName();
                $age  = $request->request->getInt('age');

                $astronaut->setName($name);
                if ($age) {
                    $astronaut->setAge($age);
                }

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($astronaut);
                $entityManager->flush();

                $data = $astronaut->toJson();

                return $this->json($responseHandler->createResponse(true, $data));
            }

            return $this->json($responseHandler->createResponse(false, "Astronaut $id not found"));
        }
    }
