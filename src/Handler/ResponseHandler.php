<?php

    namespace App\Handler;


    class ResponseHandler {
        public function createResponse($succeed, $data) {
            return [
                'succeed'=> $succeed,
                'data' => $data
            ];
        }
    }
