<?php


    namespace App\Tests\unitary;


    use App\Entity\Astronaute;
    use App\Handler\ResponseHandler;
    use PHPUnit\Framework\TestCase;

    class ResponseHandlerTest extends TestCase {
        public $responseHandler;

        public function setUp() {
            $this->responseHandler = new ResponseHandler();
        }

        public function testCreatSimpleSucceedResponse() {
            $response = $this->responseHandler->createResponse(true, 'some data');

            $this->assertEquals(['succeed' => true, 'data' => 'some data'], $response);
        }

        public function testCreatSimpleErrorResponse() {
            $response = $this->responseHandler->createResponse(false, 'some data');

            $this->assertEquals(['succeed' => false, 'data' => 'some data'], $response);
        }

        public function testCreatSucceedResponseWithObject() {
            $astronaut = new Astronaute();
            $astronaut->setName('myName')->setAge(5);

            $response = $this->responseHandler->createResponse(true, $astronaut->toJson());

            $this->assertEquals(['succeed' => true, 'data' => ['name' => 'myName', 'age' => 5]], $response);
        }

        public function testCreatErrorResponseWithObject() {
            $astronaut = new Astronaute();
            $astronaut->setName('myName')->setAge(5);

            $response = $this->responseHandler->createResponse(false, $astronaut->toJson());

            $this->assertEquals(['succeed' => false, 'data' => ['name' => 'myName', 'age' => 5]], $response);
        }
    }
